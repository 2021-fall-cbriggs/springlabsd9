<%@ include file="/WEB-INF/layouts/include.jsp" %>

<!-- <marquee><h1>Address Book</h1></marquee> -->

<!-- <h6>Address Book</h6> -->

<div id="tablediv">
	<h1>Address Book</h1>
	<!-- <button id="btnAdd" class="border" style="color: rebeccaPurple; background-color: red">
		Add Address (Addressbook)
	</button> -->
	<button id="btnAdd" class="btn btn-primary">
		Add Address
	</button>
	
	<orly-table id="table" loaddataoncreate url="<c:url value='/addressBook/getAddresses' />" 
						   includefilter maxrows="10" class="invisible" bordered>
		<orly-column field="action" label="Action" class="">
			<div slot="cell">
				<orly-icon color="cornflowerblue" name="edit" id="\${`e_\${model.id}`}"></orly-icon>
				<orly-icon color="red" name="x" id="\${`d_\${model.id}`}"></orly-icon>
			</div>
		</orly-column>
		<orly-column field="id" label="ID" class=""></orly-column>
		<orly-column field="firstName" label="First Name" class=""></orly-column>
		<orly-column field="lastName" label="Last Name" class=""></orly-column>
		<orly-column field="addressOne" label="Address" class=""></orly-column>
		<orly-column field="addressTwo" label="Address 2" class=""></orly-column>
		<orly-column field="city" label="City" class=""></orly-column>
		<orly-column field="state" label="State" class=""></orly-column>
		<orly-column field="zip" label="Zip Code" class=""></orly-column>
	</orly-table>
</div>
<div id="addEditDiv" class="d-none">
<%-- 	<c:if test="${not empty addressBook.id}"><h1>Edit Address</h1></c:if>
	<c:if test="${empty addressBook.id}"><h1>Add Address</h1></c:if> --%>
	<div class="row">
		<div class="col-12">
			<h1 id="addEditTitle" class="ml-0">Add Address</h1>
		</div>
	</div>
	
		<form id="addEditForm" is="orly-form">
			<input class="form-control" id="id" name="id" type="hidden">
			<div class="row">
				<div class="col-12 col-sm-8 col-md-6 col-lg-4">
					<div class="form-group">
						<label for="firstName">First Name</label>
						<input required class="form-control" id="firstName" name="firstName" type="text">
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-12 col-sm-8 col-md-6 col-lg-4">
					<div class="form-group">
						<label for="lastName">Last Name</label>
						<input required class="form-control" id="lastName" name="lastName" type="text">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-sm-8 col-md-6 col-lg-4">		
					<div class="form-group">
						<label for="addressOne">Address One</label>
						<input required class="form-control" id="addressOne" name="addressOne" type="text">
					</div>
				</div>	
			</div>
			<div class="row">
				<div class="col-12 col-sm-8 col-md-6 col-lg-4">		
					<div class="form-group">
						<label for="addressTwo">Address Two</label>
						<input class="form-control" id="addressTwo" name="addressTwo" type="text">
					</div>
				</div>
			</div>
					
			<div class="row">
				<div class="col-12 col-sm-8 col-md-6 col-lg-4">		
					<div class="form-group">
						<label for="city">City</label>
						<input required class="form-control" id="city" name="city" type="text">
					</div>
				</div>
			</div>
					
			<div class="row">
				<div class="col-12 col-sm-8 col-md-6 col-lg-4">		
					<div class="form-group">
						<label for="state">State</label>
						<input required class="form-control" id="state" name="state" type="text">
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-12 col-sm-8 col-md-6 col-lg-4">		
					<div class="form-group">
						<label for="zip">Zip</label>
						<input required class="form-control" id="zip" name="zip" type="text">
					</div>
				</div>
			</div>
			<!-- Group of Buttons - Strategy 1 -->		
			<div class="row">
				<div class="col-12 col-sm-4 col-md-2">
					<button id="btnSubmit" type="button" class="btn btn-primary w-100 mb-1">Submit</button>
				</div>
				<div class="col-12 col-sm-4 col-md-2">
					<button id="btnReset" type="button" class="btn btn-danger w-100 mb-1">Reset</button>
				</div>
				<div class="col-12 col-sm-4 col-md-2">
					<button id="btnCancel" type="button" class="btn btn-warning w-100 mb-1">Cancel</button>		
				</div>
			</div>	
			<!-- Bootstrap 4 Button Group - Strategy 2 ... meh -->
<!-- 			<div class="btn-group" role="group" aria-label="Basic example">
				<button id="btnSubmit" type="button" class="btn btn-primary">Submit</button>
				<button id="btnReset" type="button" class="btn btn-danger">Reset</button>
				<button id="btnCancel" type="button" class="btn btn-warning">Cancel</button>
			</div> -->
		</form>
</div>
<script>
orly.ready.then(()=> {
	orly.on(orly.qid("btnAdd"), "click", ()=> {
		// Set the form title
		orly.qid("addEditTitle").innerHTML = "Add Address";
		
		// Clear the form fields
		orly.qid("btnReset").click();
		
		// Show the form and hide the table
		show("addEditDiv");
		hide("tablediv");
	});
	
	orly.on(orly.qid("btnCancel"), "click", ()=> {
		// Show the table and hide the form
		show("tablediv");
		hide("addEditDiv");
	});
	
	orly.on(orly.qid("btnSubmit"), "click", () => {
		submitForm();
	});
	
	orly.on(orly.qid("table"), "click", (e) => {
		if (e.target.tagName == "ORLY-ICON"){
			let targetId = e.target.id;
			if (targetId.startsWith("e_")){
				//alert("ready to edit id = " + targetId.slice(2) + "!");
				editRow(targetId.slice(2));
			}
			if (targetId.startsWith("d_")){
				//alert("ready to delete id = " + targetId.slice(2) + "!");
				deleteRow(targetId.slice(2));
			}
		}
	});
});

function show(id) {
	//(orly.qid(id).classList.contains("d-none")) ? orly.qid(id).classList.remove("d-none") : null ;
	orly.qid(id).classList.remove("d-none");
}
	
function hide(id) {
	//(!orly.qid(id).classList.contains("d-none")) ? orly.qid(id).classList.add("d-none") : null ;
	orly.qid(id).classList.add("d-none");
}

function submitForm() {
	let url = `<c:url value="/addressBook/save"/>`;
	let data = orly.qid("addEditForm").values;
	
	let options = {
		method: 'POST',
		body: JSON.stringify(data),
		headers: {"Content-Type": "application/json"}
	};
	
	fetch(url, options)
	.then(response => {
		if (response.ok) {
			return response.json();
		} else {
			orlyAlert("danger", "NOT OK this is AWFUL i hate u");
		}
	}).then(data => {
		orly.qid("table").loadData();
		orlyAlert(data.messageType, data.message);
		show("tablediv");
		hide("addEditDiv");
	}).catch(e => {
		orlyAlert("danger", "NOT OK this is AWFUL i hate u" + e);
	});
}

function editRow(id){
	let url = `<c:url value="/addressBook/getAddressById"/>`;
	url += "?id=" + id;
	
	fetch(url)
	.then(response => {
		if (response.ok) {
			return response.json();
		} else {
			orlyAlert("danger", "NOT OK this is AWFUL i hate u");
		}
	}).then(Address => {
		const form = orly.q("form");
		form.values = Address;
		orly.qid("addEditTitle").innerHTML = "Edit Address";
		show("addEditDiv");
		hide("tablediv");
	}).catch(e => {
		orlyAlert("danger", "Can't edit" + e);
	});	
}

function deleteRow(id){
	let url = `<c:url value="/addressBook/delete"/>`;
	url += "?id=" + id;
	
	fetch(url)
	.then(response => {
		if (response.ok) {
			return response.json();
		} else {
			orlyAlert("danger", "NOT OK this is AWFUL i hate u");
		}
	}).then(Message => {
		orly.qid("table").loadData();
		orlyAlert(Message.messageType, Message.message);
	}).catch(e => {
		orlyAlert("danger", "Can't delete" + e);
	});
}

function orlyAlert(type, message) {
	orly.qid("alerts").createAlert({type:type, duration:"3000", msg:message});
}

</script>