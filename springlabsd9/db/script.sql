-- CREATE SCHEMA `address_book` DEFAULT CHARACTER SET utf8mb4 ;

DROP TABLE IF EXISTS address_book;

CREATE TABLE address_book (
	id Integer AUTO_INCREMENT,
	first_name varchar(256) NOT NULL,
    last_name varchar(256) NOT NULL,
    address_one	varchar(256) NOT NULL,
    address_two varchar(256),
    city varchar(256) NOT NULL,
    state varchar(256) NOT NULL,
    zip varchar(256) NOT NULL,
    primary key(id)
);

Insert into address_book(first_name, last_name, address_one, address_two, city, state, zip) values
	("Jeff", "Grusky", "123 S Main St", "Apt 2", "Springfield", "Missouri", "65802"),
    ("Luther", "Willis", "3458 Dogwoord Road", "", "Phoenix", "Arizona", "85008");
    
    
